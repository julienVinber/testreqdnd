# Créer projet

````
symfony new dnd --version=lts
````

````
cd dnd
````

# Installer lib

Ajouter dans composer.json :

````
    "repositories": [
        {
            "type": "vcs",
            "url": "https://gitlab.com/julienVinber/testreqdnd.git"
        }
    ],
````
	
lancer
````
composer req phpunit/phpunit dnd/product_import:dev-master
````

# Test fonctionalité

````
php bin/console dnd:p:i vendor/dnd/product_import/tests/resources/products.csv

````
````
php bin/console dnd:p:i -json vendor/dnd/product_import/tests/resources/products.csv
````
# Test unitaire


````
php ./vendor/phpunit/phpunit/phpunit --bootstrap ./tests/bootstrap.php  ./vendor/dnd/product_import/tests
````