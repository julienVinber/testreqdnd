<?php

namespace Dnd\ProductImport\Tests\Factory;


use Dnd\ProductImport\Factory\ProductFactory;
use PHPUnit\Framework\TestCase;

class ProductFactoryTest extends TestCase
{
    public function testFromCSVArray_enabled()
    {
        $data = ['t1', 't2', '1', 10.59, 't5', 't6', '2020-01-02 03:04'];

        $product = ProductFactory::fromCSVArray($data);

        $this->assertEquals('t1', $product->getSku());
        $this->assertEquals('t2', $product->getTitle());
        $this->assertEquals(true, $product->isEnabled());
        $this->assertEquals(10.59, $product->getPrice());
        $this->assertEquals('t5', $product->getCurrency());
        $this->assertEquals('t6', $product->getDescription());
        $this->assertEquals(new \DateTime('2020-01-02 03:04'), $product->getCreatedAt());
    }
    public function testFromCSVArray_disabled()
    {
        $data = ['t1', 't2', '0', 10.59, 't5', 't6', '2020-01-02 03:04'];

        $product = ProductFactory::fromCSVArray($data);

        $this->assertEquals('t1', $product->getSku());
        $this->assertEquals('t2', $product->getTitle());
        $this->assertEquals(false, $product->isEnabled());
        $this->assertEquals(10.59, $product->getPrice());
        $this->assertEquals('t5', $product->getCurrency());
        $this->assertEquals('t6', $product->getDescription());
        $this->assertEquals(new \DateTime('2020-01-02 03:04'), $product->getCreatedAt());
    }
}