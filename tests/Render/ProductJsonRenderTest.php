<?php

namespace Dnd\ProductImport\Tests\Render;

use Dnd\ProductImport\Entity\Product;
use Dnd\ProductImport\Render\ProductJsonRender;
use Dnd\ProductImport\Tools\Slugify;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Console\Output\OutputInterface;

class ProductJsonRenderTest extends TestCase
{
    private $retour='';
    private $stub;

    protected $product_enabled;
    protected $product_disabled;

    const JSON_ENABLED = '[{"Sku":"t3","Status":"Enable","Price":"10,00 €","Description":"t4","Create_At":"Thursday, 03-Feb-2000 04:05:00 UTC","Slug":"t2",}]';
    const JSON_DISABLED = '[{"Sku":"t3","Status":"Disable","Price":"10,00 €","Description":"t4","Create_At":"Thursday, 03-Feb-2000 04:05:00 UTC","Slug":"t2",}]';

    public function __construct(?string $name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);

        $this->product_enabled = (new Product())
            ->setTitle('t1')
            ->setCurrency('€')
            ->setSlug('t2')
            ->setPrice('10')
            ->setSku('t3')
            ->setDescription('t4')
            ->setEnabled(true)
            ->setCreatedAt(new \DateTime('2000-02-03 04:05'));

        $this->product_disabled = (new Product())
            ->setTitle('t1')
            ->setCurrency('€')
            ->setSlug('t2')
            ->setPrice('10')
            ->setSku('t3')
            ->setDescription('t4')
            ->setEnabled(false)
            ->setCreatedAt(new \DateTime('2000-02-03 04:05'));


        $this->stub = $this->createMock(OutputInterface::class);
        $this->stub->expects($this->any())->method('write')->willReturnCallback(
            function ($val)
            {
                $this->retour .= $val;
            });
    }

    public function testRender_enabled()
    {
        $this->retour = '';
        ProductJsonRender::render($this->stub, [$this->product_enabled]);
        $this->assertEquals(self::JSON_ENABLED, $this->retour);
    }

    public function testRender_disabled()
    {
        $this->retour = '';
        ProductJsonRender::render($this->stub, [$this->product_disabled]);
        $this->assertEquals(self::JSON_DISABLED, $this->retour);
    }
}