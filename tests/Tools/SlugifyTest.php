<?php

namespace Dnd\ProductImport\Tests\Tools;

use Dnd\ProductImport\Tools\Slugify;
use PHPUnit\Framework\TestCase;

class SlugifyTest extends TestCase
{    
    public function testtoSlug_Majuscul()
    {
        $this->assertEquals('slugify', Slugify::toSlug('Slugify'));
        $this->assertEquals('slugify', Slugify::toSlug('slugifY'));
        $this->assertEquals('slugify', Slugify::toSlug('SLUGIFY'));
    }

    public function testtoSlug_Espace()
    {
        $this->assertEquals('_slugify', Slugify::toSlug(' slugify'));
        $this->assertEquals('slugify_', Slugify::toSlug('slugify '));
        $this->assertEquals('slu_gify', Slugify::toSlug('slu gify'));
    }

    public function testtoSlug_Speciaux()
    {
        $this->assertEquals('-slugify', Slugify::toSlug('#slugify'));
        $this->assertEquals('slugify-', Slugify::toSlug('slugify#'));
        $this->assertEquals('slu-gify', Slugify::toSlug('slu#gify'));
        $this->assertEquals('slu--gify', Slugify::toSlug('slu##gify'));
    }
    
}