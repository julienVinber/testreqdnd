<?php

namespace Dnd\ProductImport\Tests\Loader;

use Dnd\ProductImport\Loader\ProductCSVLoader;
use PHPUnit\Framework\TestCase;

class ProductCSVLoaderTest extends TestCase
{
    public function testProductGenerator_firstLine()
    {
        $gen = new ProductCSVLoader();
        $data = $gen->productGenerator(__DIR__ . '/../resources/products.csv');

        $product = $data->current();
        $this->assertEquals('628937273', $product->getSku());
        $this->assertEquals('Cornelia, the dark unicorn', $product->getTitle());
        $this->assertEquals('14.87', $product->getPrice());
        $this->assertEquals('€', $product->getCurrency());
        $this->assertEquals('Cornelia, \rThe Dark Unicorn.', $product->getDescription());
        $this->assertEquals(new \DateTime('2018-12-12 10:34:39'), $product->getCreatedAt());
        $this->assertEquals('cornelia-_the_dark_unicorn', $product->getSlug());

        $data->next();
        $product = $data->current();
        $this->assertEquals('722821313', $product->getSku());
    }
    public function testProductGenerator_nextLine()
    {
        $gen = new ProductCSVLoader();
        $data = $gen->productGenerator(__DIR__ . '/../resources/products.csv');
        
        $data->next();
        $product = $data->current();
        $this->assertEquals('722821313', $product->getSku());
    }
}
