<?php


namespace Dnd\ProductImport\Loader;


use Dnd\ProductImport\Factory\ProductFactory;
use Exception;
use Generator;

class ProductCSVLoader
{

    /**
     * @param $file
     * @return Generator|null
     */
    public function productGenerator($file): ?Generator
    {
        $handle = fopen($file, 'r');

        fgetcsv($handle); //On le lance une fois pour supprimer l'entete.

        while (!feof($handle)) {
            try {
                $data = fgetcsv($handle, null, ';');
                yield ProductFactory::fromCSVArray($data);
            } catch (Exception $e) {
                //On ignore les lignes si elle ne passe pas
            }
        }

        fclose($handle);
    }
}