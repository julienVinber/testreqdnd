<?php


namespace Dnd\ProductImport\Command;


use Dnd\ProductImport\Loader\ProductCSVLoader;
use Dnd\ProductImport\Render\ProductJsonRender;
use Dnd\ProductImport\Render\ProductTableRender;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ImportProductsCommand extends Command
{
    protected static $defaultName = 'dnd:products:import';

    /**
     * @var LoggerInterface
     */
    private $logger;

    public function __construct(?LoggerInterface $logger = null)
    {
        $this->logger = $logger;
        parent::__construct();
    }

    protected function configure(): void
    {
        $this->setDescription('Traitement du flux de donnée produit CSV entrant.')
            ->addArgument('csv', InputArgument::REQUIRED, 'Le fichier à importer.')
            ->addOption('json', 'j', InputOption::VALUE_OPTIONAL, 'Sortie JSON.', false);
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $file = $input->getArgument('csv');
        $json = $input->getOption('json');

        if (!file_exists($file)) {
            if ($this->logger !== null) {
                $this->logger->error('Le fichier "' . $file . '" n\'existe pas.');
            }
            return 1;
        }

        //Récupération données
        $loader = new ProductCSVLoader();
        $data = $loader->productGenerator($file);

        //Rendu
        if (!$json) {
            ProductTableRender::render($output, $data);
        } else {
            ProductJsonRender::render($output, $data);
        }

        return 0;
    }
}