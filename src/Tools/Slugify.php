<?php


namespace Dnd\ProductImport\Tools;


class Slugify
{

    /**
     * @param string $slug
     * @return string
     */
    public static function toSlug(string $slug): string
    {
        $slug = strip_tags($slug);
        $slug = mb_strtolower($slug);
        $slug = preg_replace('/[\s]+/', '_', $slug);
        $slug = preg_replace('/\W/', '-', $slug);
        return $slug;
    }
}