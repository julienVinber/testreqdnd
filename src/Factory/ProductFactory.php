<?php


namespace Dnd\ProductImport\Factory;


use Dnd\ProductImport\Entity\Product;
use Dnd\ProductImport\Tools\Slugify;
use DateTime;

class ProductFactory
{
    private const CSV_SKU = 0;
    private const CSV_TITLE = 1;
    private const CSV_ENABLED = 2;
    private const CSV_PRICE = 3;
    private const CSV_CURRENCY = 4;
    private const CSV_DESCRIPTION = 5;
    private const CSV_CREATEAT = 6;

    /**
     * @param array $data
     * @return Product
     * @throws \Exception
     */
    public static function fromCSVArray(array $data): Product
    {
        return (new Product())
            ->setSku($data[self::CSV_SKU])
            ->setTitle($data[self::CSV_TITLE])
            ->setEnabled($data[self::CSV_ENABLED] === '1')
            ->setPrice((float)$data[self::CSV_PRICE])
            ->setCurrency($data[self::CSV_CURRENCY])
            ->setDescription($data[self::CSV_DESCRIPTION])
            ->setCreatedAt(new DateTime($data[self::CSV_CREATEAT]))
            ->setSlug(Slugify::toSlug($data[self::CSV_TITLE]));
    }
}