<?php


namespace Dnd\ProductImport\Render;


use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Output\OutputInterface;

class ProductTableRender implements RenderInterface
{

    public static function render(OutputInterface $output, iterable $products): void
    {
        $table = new Table($output);
        $table->setHeaders([
            'Sku',
            'Status',
            'Price',
            'Description',
            'Created At',
            'Slug'
        ]);
        foreach ($products as $product) {
            $table->addRow([
                $product->getSku(),
                $product->isEnabled() ? 'Enable' : 'Disable',
                number_format($product->getPrice(), 2, ',', ' ') . ' €',
                str_replace('<br/>', "\n", $product->getDescription()),
                $product->getCreatedAt()->format('l, d-M-Y H:i:s e'),
                $product->getSlug()
            ]);
        }
        $table->render();
    }
}