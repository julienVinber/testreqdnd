<?php


namespace Dnd\ProductImport\Render;


use Symfony\Component\Console\Output\OutputInterface;

interface RenderInterface
{
    public static function render(OutputInterface $output, iterable $products): void;
}