<?php


namespace Dnd\ProductImport\Render;


use Dnd\ProductImport\Entity\Product;
use Symfony\Component\Console\Output\OutputInterface;

class ProductJsonRender implements RenderInterface
{

    public static function render(OutputInterface $output, iterable $products): void
    {
        $output->write('[');

        $first = true;
        foreach ($products as $product) {
            if (!$first) {
                $output->write(',');
            } else {
                $first = false;
            }
            $output->write(self::buildRow($product));
        }
        $output->write(']');
    }

    private static function buildRow(Product $product): string
    {
        return '{' .
            self::buildCol('Sku', $product->getSku()) . ',' .
            self::buildCol('Status', $product->isEnabled() ? 'Enable' : 'Disable') . ',' .
            self::buildCol('Price', number_format($product->getPrice(), 2, ',', ' ') . ' €') . ',' .
            self::buildCol('Description', str_replace('<br/>', '\r', $product->getDescription())) . ',' .
            self::buildCol('Create_At', $product->getCreatedAt()->format('l, d-M-Y H:i:s e')) . ',' .
            self::buildCol('Slug', $product->getSlug()) . ',' .
            '}';
    }

    private static function buildCol(string $key, string $value): string
    {
        return '"' . $key . '":"' . $value . '"';
    }
}